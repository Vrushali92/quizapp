//
//  QuestionViewController.swift
//  QuizApp
//
//  Created by Vrushali Kulkarni on 02/07/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import UIKit

final class QuestionViewController: UIViewController {

    typealias SelectionCallBack = ([String]) -> Void
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!

    private var question = ""
    private var options = [String]()
    private var selection: SelectionCallBack? = nil

    private let cellIdentifier = "Cell"

    convenience init(
        question: String,
        options: [String],
        selection: @escaping SelectionCallBack
    ) {
        self.init()
        self.question = question
        self.options = options
        self.selection = selection
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        headerLabel.text = question
    }
}

extension QuestionViewController {

    private func selectedOptions(in tableView: UITableView) -> [String] {
        guard let indexPath = tableView.indexPathsForSelectedRows else { return [] }
        return indexPath.map{ options[$0.row] }
    }

    private func dequeueCell(in tableView: UITableView) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) else {
            return UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        return cell
    }
}

extension QuestionViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        options.count
    }

    func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let cell = dequeueCell(in: tableView)
        cell.textLabel?.text = options[indexPath.row]
        return cell
    }

    func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {
        selection?(selectedOptions(in: tableView))
    }

    func tableView(
        _ tableView: UITableView,
        didDeselectRowAt indexPath: IndexPath
    ) {
        if tableView.allowsMultipleSelection {
            selection?(selectedOptions(in: tableView))
        }
    }
}
